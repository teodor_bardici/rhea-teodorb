﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RHEA_Test_Assignment
{
    public interface IAccountService
    {
        Task<double> GetAccountAmount(int accountId);
    }
}
