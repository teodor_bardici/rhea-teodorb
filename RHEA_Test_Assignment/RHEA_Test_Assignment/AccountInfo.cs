﻿using System.Threading;
using System.Threading.Tasks;

namespace RHEA_Test_Assignment
{
    public class AccountInfo
    {
        // we'll use a semaphore to account for potential race conditions on Amount.
        // However, the problem statement is not very clear. 
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);
        private readonly int _accountId;
        private readonly IAccountService _accountService;

        public AccountInfo(int accountId, IAccountService accountService)
        {
            _accountId = accountId;
            _accountService = accountService;
        }

        public double Amount { get; private set; }

        public async Task RefreshAmount()
        {
            try
            {
                await _semaphore.WaitAsync();
                Amount = await _accountService.GetAccountAmount(_accountId);
            }
            finally
            {
                _semaphore.Release();
            }
        }
    }
}
