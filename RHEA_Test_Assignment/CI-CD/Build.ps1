$solution = "..\RHEA_Test_Assignment.sln"

dotnet build $solution

if(!$?) {
	Write-Output "Build failed!"
	exit
}

dotnet test $solution

if(!$?) {
	Write-Output "Unit tests failed!"
	exit
}


dotnet pack $solution

if(!$?) {
	Write-Output "Nuget package creation failed!"
	exit
}

Write-Output "Success!"