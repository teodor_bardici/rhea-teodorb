using FluentAssertions;
using Moq;
using RHEA_Test_Assignment;
using System.Threading.Tasks;
using Xunit;

namespace RHEA_Test_Assignment_Tests
{
    public class AccountInfoTests
    {
        [Fact]
        public async Task RefreshAmount_Should_GetAmountFromAccountService()
        {
            //Arrange
            var accountId = 1234;
            var accountAmount = 99;
            var accountServiceMock = new Mock<IAccountService>();
            accountServiceMock.Setup(mock => mock.GetAccountAmount(It.Is<int>(id => id == accountId))).ReturnsAsync(accountAmount);
            var accountInfo = new AccountInfo(accountId, accountServiceMock.Object);

            //Act
            await accountInfo.RefreshAmount();

            //Assert
            //verify that the service was called
            accountServiceMock.Verify(mock => mock.GetAccountAmount(accountId), Times.Once);

            //verify that after the call, the account amount is the same as the value that the service returns.
            accountInfo.Amount.Should().Be(accountAmount);
        }
    }
}
